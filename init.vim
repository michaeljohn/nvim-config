call plug#begin("~/.vim/plugged")

" Theme
Plug 'arcticicestudio/nord-vim'
Plug 'ntk148v/vim-horizon'
Plug 'junegunn/seoul256.vim'
Plug 'ulwlu/elly.vim'
Plug 'co1ncidence/gunmetal.vim'
Plug 'co1ncidence/itai.vim'
Plug 'co1ncidence/sunset.vim'
Plug 'co1ncidence/mountaineer.vim'
Plug 'casperstorm/sort-hvid.vim'
Plug 'pradyungn/Mountain', {'rtp': 'vim'}
Plug 'savq/melange'

" Handy stuff
Plug 'sheerun/vim-polyglot'
Plug 'preservim/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdcommenter'
Plug 'ryanoasis/vim-devicons'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'ntpeters/vim-better-whitespace'
Plug 'tpope/vim-commentary'
Plug 'dangerousHobo/vim-linux-coding-style'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/limelight.vim'
Plug 'psliwka/vim-smoothie'
Plug 'chrisbra/vim-kconfig'
Plug 'moll/vim-bbye'

Plug 'xavierchow/vim-swagger-preview'

" Wiki
Plug 'vimwiki/vimwiki'

" Go stuff
Plug 'fatih/vim-go'
Plug 'nsf/gocode'

" Elvish
Plug 'dmix/elvish.vim', { 'on_ft': ['elvish', 'elv'] }

" Dhall
Plug 'vmchale/dhall-vim'

" Zig
Plug 'ziglang/zig.vim'

" Language Server
Plug 'neoclide/coc.nvim', {'branch': 'v0.0.78'}

call plug#end()

" Config section
if (has("termguicolors"))
	set termguicolors
endif

set encoding=UTF-8
set nu rnu
set autoindent
set smarttab
set tabstop=4
set shiftwidth=4
set expandtab
set list
set listchars=tab:>\ ,trail:-,eol:↵
set showmatch

" required for vimwiki
set nocompatible

augroup numbertoggle
      autocmd!
      autocmd BufEnter,FocusGained,InsertLeave * set rnu
      autocmd BufLeave,FocusLost,InsertEnter * set nornu
augroup END

syntax enable

" load/save folding
set viewoptions-=options
"augroup folding
"    autocmd BufWinLeave *.* if expand('%') != '' && &buftype !~ 'nofile' | mkview | endif
"    autocmd BufWinEnter *.* if expand('%') != '' && &buftype !~ 'nofile' | silent! loadview | endif
"augroup END

" required for nerdcommenter
filetype plugin on

" colorscheme nord
" colorscheme horizon
set background=dark
let g:seoul256_background = 234
colorscheme seoul256
" colorscheme gunmetal-grey
" colorscheme paper
colorscheme elly

" lightline
let g:lightline = {}
let g:lightline.colorscheme = 'horizon'
let g:lightline.colorscheme = 'seoul256'
let g:lightline.colorscheme = 'paper'
let g:lightline.colorscheme = 'elly'

" Commentary
autocmd FileType elv setlocal commentstring=#\ %s

" Integrated Term
" open new split panes to right and below
set splitright
set splitbelow
" turn terminal to normal mode with escape
tnoremap <Esc> <C-\><C-n>
" start terminal in insert mode
au BufEnter * if &buftype == 'terminal' | :startinsert | endif
" open terminal on ctrl+n
function! OpenTerminal()
  split term://bash
  resize 10
endfunction
nnoremap <c-n> :call OpenTerminal()<CR>

" fzf
let g:fzf_preview_window = ['right:40%:hidden', 'ctrl-/']

" markdown-preview
let g:mkdp_browser = 'firefox'

